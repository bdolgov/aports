# Contributor: Sodface <sod@sodface.com>
# Maintainer: Sodface <sod@sodface.com>
pkgname=satty
pkgver=0.10.0
pkgrel=0
pkgdesc="Screenshot annotation tool"
url="https://github.com/gabm/Satty"
arch="all"
license="MPL-2.0"
makedepends="
	cargo
	cargo-auditable
	gtk4.0-dev
	libadwaita-dev
	"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
options="!check" # no test suite
source="$pkgname-$pkgver.tar.gz::https://github.com/gabm/Satty/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/Satty-$pkgver"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

package() {
	install -Dm755 target/release/satty -t "$pkgdir"/usr/bin

	install -Dm644 satty.desktop \
		-t "$pkgdir"/usr/share/applications/
	install -Dm644 assets/satty.svg \
		-t "$pkgdir"/usr/share/icons/hicolor/scalable/apps

	install -Dm644 completions/satty.bash \
		"$pkgdir"/usr/share/bash-completion/completions/satty
	install -Dm644 completions/satty.fish \
		-t "$pkgdir"/usr/share/fish/vendor_completions.d
	install -Dm644 completions/_satty \
		-t "$pkgdir"/usr/share/zsh/site-functions
}

sha512sums="
9a456269118068e5dcf2f4a60e0424415f9025e03010d42702856993754147b41a3762c5ec3855e28591156451cc29170e0d304b136465b73ee239127845e780  satty-0.10.0.tar.gz
"
